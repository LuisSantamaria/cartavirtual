package com.example.lucho.mistersantamara;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class MenuActivity extends Activity {

    Button entradas;
    Button platosFuertes;
    Button bebidas;
    Button postres;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        entradas = (Button)findViewById(R.id.bt1);
        entradas.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent entradas = new Intent(MenuActivity.this, MenuEntradasActivity.class);
                startActivity(entradas);
            }
        });

        platosFuertes = (Button)findViewById(R.id.bt2);
        platosFuertes.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent platosFuertes = new Intent(MenuActivity.this, MenuPlatosFuertesActivity.class);
                startActivity(platosFuertes);
            }
        });

        bebidas = (Button)findViewById(R.id.bt3);
        bebidas.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent bebidas = new Intent(MenuActivity.this, MenuBebidasActivity.class);
                startActivity(bebidas);
            }
        });

        postres = (Button)findViewById(R.id.bt4);
        postres.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent postres = new Intent(MenuActivity.this, MenuPostresActivity.class);
                startActivity(postres);
            }
        });

    }
}
