package com.example.lucho.mistersantamara;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity
{


    Button mapa;
    Button menu;
    Button reservar;




    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

           mapa = (Button)findViewById(R.id.btn1);
           mapa.setOnClickListener(new View.OnClickListener()
           {
               @Override
               public void onClick(View view)
               {
                   Intent mapa = new Intent(MainActivity.this, MapsActivity.class);
                   startActivity(mapa);
               }
           });

           menu = (Button)findViewById(R.id.btn2);
           menu.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   Intent menu = new Intent(MainActivity.this, MenuActivity.class);
                   startActivity(menu);
               }
           });



           reservar = (Button)findViewById(R.id.btn3);
           reservar.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   Uri whatsApp = Uri.parse("https://api.whatsapp.com/send?phone=573195087871");
                   Intent reservar = new Intent(Intent.ACTION_VIEW,whatsApp);
                   startActivity(reservar);
               }
           });
    }


}
