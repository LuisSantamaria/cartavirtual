package com.example.lucho.mistersantamara;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class MenuPlatosFuertesActivity extends Activity {

    ListView listaDatos;
    ArrayList<Datos> Lista;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_platos_fuertes);

        listaDatos = (ListView)findViewById(R.id.listaDatos);

        Lista = new ArrayList<Datos>();

        Lista.add(new Datos("Delicioso pan francés relleno con camarones al curry. \n$ 5.990",1,R.drawable.platofuertepanecookcamaronespng,"Pane Cook de Camarones"));
        Lista.add(new Datos("Típica preparación del ají de gallina limeño, aceitunas moradas, con un ligero toque picante.\n$ 5.990",2,R.drawable.platofuertepolloperuanopng,"Pollo Peruano"));
        Lista.add(new Datos("Mezcla de sabores mexicanos con palta y salsa de queso.\n$ 5.790",3,R.drawable.platofuertepollotrufamexicanapng,"Pollo de Trufa Mexicana"));
        Lista.add(new Datos("Mozarella de Búfala, tomates secos, jamón serrano, cebollas caramelizadas y rúgula.\n$ 5.890",4,R.drawable.platofuerteserrano,"Serrano"));
        Lista.add(new Datos("Queso, champiñones frescos, tomate y albahaca fresca y salsa de champiñones.\n$ 4.890",5,R.drawable.platofuertetoscana,"Toscana"));
        Lista.add(new Datos("Julianas de filete y champiñones en su salsa.\n$ 5.490",6,R.drawable.platofuertetroganoffpng,"Troganoff"));



        Adaptador miadaptador = new Adaptador(getApplicationContext(),Lista);

        listaDatos.setAdapter(miadaptador);
    }
}
