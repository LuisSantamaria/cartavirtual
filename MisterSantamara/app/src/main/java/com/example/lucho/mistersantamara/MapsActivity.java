package com.example.lucho.mistersantamara;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng upb = new LatLng(6.2425369, -75.5914277);
        LatLng molinos = new LatLng(6.23317, -75.6063687);
        LatLng premium_plaza = new LatLng(6.2292368, -75.57273);
        LatLng santa_fe = new LatLng(6.1966335, -75.5762681);

        LatLng bogota = new LatLng(4.7022085, -74.0441784);

        mMap.addMarker(new MarkerOptions().position(santa_fe).title("Mr Santamaría"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(santa_fe));

        mMap.addMarker(new MarkerOptions().position(premium_plaza).title("Mr Santamaría"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(premium_plaza));

        mMap.addMarker(new MarkerOptions().position(bogota).title("Mr Santamaría"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(bogota));

        mMap.addMarker(new MarkerOptions().position(molinos).title("Mr Santamaría"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(molinos));


        mMap.addMarker(new MarkerOptions().position(upb).title("Mr Santamaría"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(upb));
    }
}
