package com.example.lucho.mistersantamara;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.widget.ListView;

import java.util.ArrayList;

public class MenuBebidasActivity extends Activity {

    ListView listaDatos;
    ArrayList<Datos> Lista;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_bebidas);


        listaDatos = (ListView)findViewById(R.id.listaDatos);

        Lista = new ArrayList<Datos>();

        Lista.add(new Datos("Es un vaso con agua, pero no cualquier vaso, este tiene agua de manantial. \n$ 0",1,R.drawable.bebidaagua,"Vaso con Agua"));
        Lista.add(new Datos("Hecho de mango, maracuyá y piña.\n$ 2600",2,R.drawable.bebidaalegria,"Jugo Alegría"));
        Lista.add(new Datos("La espuma del capuccino adornado por el dulce del caramelo.\n$ 2.090",3,R.drawable.bebidacapuccinocaramel,"Capuccino Caramel"));
        Lista.add(new Datos("El aroma del café y la intensidad del chocolate.\n$ 1.600",4,R.drawable.bebidachocochino,"Chocochino"));
        Lista.add(new Datos("Es jugo, pero de uva.\n$ 2.200",5,R.drawable.bebidajugodeuva,"Jugo de Uva"));
        Lista.add(new Datos("Limonada, deliciosa limonada de cocos traídos de San Andrés.\n$ 2.400",6,R.drawable.bebidalimonadadecoco,"Limonada de Coco"));
        Lista.add(new Datos("Ahora además de disfrutar de una buena limonada disfrutarás de un buen aliento.\n$ 2.400",7,R.drawable.bebidalimonadadehierbabuena,"Limonada de Hierbabuena"));



        Adaptador miadaptador = new Adaptador(getApplicationContext(),Lista);

        listaDatos.setAdapter(miadaptador);



    }
}
