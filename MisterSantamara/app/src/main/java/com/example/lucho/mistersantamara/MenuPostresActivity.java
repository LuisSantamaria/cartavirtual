package com.example.lucho.mistersantamara;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class MenuPostresActivity extends Activity {

    ListView listaDatos;
    ArrayList<Datos> Lista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_postres);

        listaDatos = (ListView)findViewById(R.id.listaDatos);

        Lista = new ArrayList<Datos>();

        Lista.add(new Datos("$ 5.990",1,R.drawable.postrebananaroyal,"Copa Banana Royal"));
        Lista.add(new Datos("$ 5.990",2,R.drawable.postrebrasilia,"Copa Brasilia"));
        Lista.add(new Datos("$ 5.790",3,R.drawable.postrebrownie,"Copa Brownie"));
        Lista.add(new Datos("$ 5.890",4,R.drawable.postrecopacaramel,"Copa Caramel"));
        Lista.add(new Datos("$ 4.890",5,R.drawable.postredamablanca,"Dama Blanca"));
        Lista.add(new Datos("$ 5.490",6,R.drawable.postreronconpasas,"Copa Ron con Pasas"));
        Lista.add(new Datos("$ 5.290",7,R.drawable.postretentacion,"Copa Tentación"));
        Lista.add(new Datos("$ 2.490",8,R.drawable.postretiramisu,"Postre Tiramisú"));
        Lista.add(new Datos("$ 4.490",9,R.drawable.postrewaffledenutellaybanano,"Waffle de Nutella y Banano"));
        Lista.add(new Datos("$ 2.290",10,R.drawable.postreconosdehelado,"Cono de helado"));





        Adaptador miadaptador = new Adaptador(getApplicationContext(),Lista);

        listaDatos.setAdapter(miadaptador);
    }
}
