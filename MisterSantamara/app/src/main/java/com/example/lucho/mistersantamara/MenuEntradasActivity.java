package com.example.lucho.mistersantamara;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MenuEntradasActivity extends Activity
{

    ListView listaDatos;
    ArrayList<Datos> Lista;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_entradas);

         listaDatos = (ListView)findViewById(R.id.listaDatos);

         Lista = new ArrayList<Datos>();

        Lista.add(new Datos("Couscous preparado con especias, camarones marinados al curry, palta, chutney de mango y ensalada verde. \n$ 5.990",1,R.drawable.ensaladamarroqui,"Ensalada Marroqui"));
        Lista.add(new Datos("Camarones, calamares, pesto, variedad de lechugas, apio, champiñones, aceitunas negras, ceboola, vinagre balsámico y aceite de oliva.\n$ 5.990",2,R.drawable.ensaladamediterranea,"Ensalada Mediterranea"));
        Lista.add(new Datos("Pechuga de pavo asada, variedad de lechugas frescas, apio, champiñones, cebolla y sésamo en una vinagreta oriental.\n$ 5.790",3,R.drawable.ensaladathai,"Ensalada Thai"));
        Lista.add(new Datos("Mozzarella de búfala envuelta en jamón serrano, con cebollas caramelizadas, rúgula y vinagreta a base de mostaza dijón.\n$ 5.890",4,R.drawable.ensaladatorina,"Ensalada Torina"));
        Lista.add(new Datos("Mezcla de tofu al curry con pimentón, mani y aromáticos sabores de la india.\n$ 4.890",5,R.drawable.gandhi,"Gandhi"));
        Lista.add(new Datos("Queso, alcachofas, aceitunas moradas, cebolla, tomate y especias.\n$ 5.490",6,R.drawable.pita_griega,"Pitha Griega"));
        Lista.add(new Datos("Espinaca, lomo kassier, cebolla, huevo, champiñones, tomate, queso y salsas.\n$ 5.290",7,R.drawable.popeyepocket,"Popeye Pocket"));



        Adaptador miadaptador = new Adaptador(getApplicationContext(),Lista);

        listaDatos.setAdapter(miadaptador);


    }
}
